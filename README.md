# Projeto pipeline de testes com docker, jest e junit
Este projeto foi criado com o objetivo de servir como documentação pessoal para consultas futuras sobre como criar uma pipeline de testes utilizando Docker, Jest e JUnit no GitLab, além de demonstrar a implantação dessa pipeline em uma instância de máquina virtual na nuvem.

<b>Observação:</b> O servidor Linux na plataforma GCP não foi criado. Este projeto foi utilizado apenas como documentação para referências em possíveis funcionalidades futuras. Portanto, a pipeline de execução apresentará uma falha devido a essa ausência.

## Passo-a-passo para configurar um gitlab runner na maquina virtual 
**Para cadastrar um GitLab Runner no GitLab, siga os passos abaixo:**

1. Instale e configure o GitLab Runner em uma máquina ou servidor que deseja usar como runner. Verifique a documentação oficial do GitLab Runner para obter instruções detalhadas de instalação e configuração específicas para o seu sistema operacional.

2. Após a instalação, você precisa registrar o runner no seu projeto GitLab. Para fazer isso, você precisa obter o token de registro do runner. Para isso, siga os passos:
    - Acesse o seu projeto no GitLab.
    - Vá para **`Settings`** (Configurações) e selecione **`CI/CD`**.
    - Role a página até encontrar a seção "Runners".
    - Copie o token exibido nessa seção.

3. No terminal da máquina onde o GitLab Runner está instalado, execute o seguinte comando para iniciar o processo de registro:

```yaml
sudo gitlab-runner register
```

4. O comando irá solicitar algumas informações para configurar o runner:
    - Insira a URL do seu GitLab (por exemplo, **`https://gitlab.com`**).
    - Cole o token de registro que você copiou anteriormente.
    - Escolha um nome para o runner.
    - Defina as tags (opcionalmente) para associar com o runner. As tags são úteis para controlar em quais jobs o runner será utilizado.
    - Escolha o executor (por exemplo, **`shell`**, **`docker`**, **`kubernetes`**, dependendo do seu ambiente).

5. Após inserir todas as informações, o GitLab Runner será registrado no seu projeto GitLab. Você verá uma mensagem confirmando o registro bem-sucedido.

6. Volte para as configurações do seu projeto GitLab no navegador e verifique a seção "Runners". O novo runner registrado deve ser exibido lá.

A partir de agora, você pode configurar seus arquivos **`.gitlab-ci.yml`** para utilizar o runner registrado. O GitLab irá enviar automaticamente os jobs de CI/CD para o runner para execução.

Repita os passos acima para registrar outros runners em diferentes máquinas ou servidores, se necessário.

Lembre-se de ajustar as configurações de acordo com o seu ambiente, como URLs, tokens, nomes de runners e tags. Consulte a documentação oficial do GitLab Runner para obter informações adicionais sobre configurações avançadas e opções de execução.
